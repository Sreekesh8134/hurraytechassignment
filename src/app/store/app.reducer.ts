import { DashboardModel, Syllabus, User } from "../common/models";
import { ActionReducerMap } from '@ngrx/store';
import { syllabusReducer } from "../screens/syllabus/syllabus.reducer";
import { dashBoardReducer } from "../screens/dashboard/dashboard.reducer";
import { userReducer } from "./user/user.reducer";

export interface AppState {
    syllabus: Syllabus;
    dashboard: DashboardModel;
    user: User;
}

export const appReducer = {
    syllabus: syllabusReducer,
    dashboard: dashBoardReducer,
    user: userReducer,
}