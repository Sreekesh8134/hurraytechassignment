import { User } from "../../common/models";
import { UserActionTypes, UserActions } from "./user.action";
const initialState: User = {
    uid: '',
    email: '',
    userName: '',
    photoURL: '',
    phoneNumber: '',
}
export function userReducer(state = initialState, action: UserActions) {

    switch (action.type) {
        case UserActionTypes.SET_USER:
            return {
                ...action.payload
            }

        case UserActionTypes.UPDATE_URL:
            return {
                ...state,
                photoURL:action.payload,
            }

        default:
            return state;
    }
}