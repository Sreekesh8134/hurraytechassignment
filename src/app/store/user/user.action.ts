import { Action } from "@ngrx/store";
import { User } from "../../common/models";

export enum UserActionTypes {
    SET_USER = "[User] Setting the user",
    UPDATE_URL = "[User] Updating Profile Url",
}

export class SetUser implements Action {
    readonly type = UserActionTypes.SET_USER;

    constructor(public payload: User) { }
}

export class UpdateUrl implements Action {
    readonly type = UserActionTypes.UPDATE_URL;

    constructor(public payload: string) { }
}

export type UserActions = SetUser | UpdateUrl;