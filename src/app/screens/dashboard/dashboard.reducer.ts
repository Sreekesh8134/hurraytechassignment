import { Action } from "@ngrx/store";
import { DashboardModel, SchoolClasses, Board, Medium, Class } from "../../common/models";
import { DashBoardActionTypes, DashBoardActions } from "./dashboard.actions";

export const initialDashboardState: DashboardModel = {
    schoolClasses: [
        // new SchoolClasses(
        //     '1615629590945',
        //     'Bhashyam',
        //     Board.SSC,
        //     Medium.ENGLISH,
        //     Class.FIFTH
        // ),
        // new SchoolClasses(
        //     '1615629591443',
        //     'Narayana',
        //     Board.CBSE,
        //     Medium.ENGLISH,
        //     Class.FOURTH
        // ),
        // new SchoolClasses(
        //     '1615629591943',
        //     'Chaitanya',
        //     Board.IB_SCHOOL,
        //     Medium.TELUGU,
        //     Class.FOURTH
        // ),
        // new SchoolClasses(
        //     '1615629592444',
        //     'Bhashyam',
        //     Board.CBSE,
        //     Medium.ENGLISH,
        //     Class.THIRD
        // ),
        // new SchoolClasses(
        //     '1615629592944',
        //     'Narayana',
        //     Board.CBSE,
        //     Medium.HINDI,
        //     Class.FOURTH
        // ),
        // new SchoolClasses(
        //     '1615629593443',
        //     'DPS',
        //     Board.CBSE,
        //     Medium.ENGLISH,
        //     Class.FOURTH
        // ),
        // new SchoolClasses(
        //     '1615629593943',
        //     'Bhashyam',
        //     Board.SSC,
        //     Medium.ENGLISH,
        //     Class.ONE
        // ),
        // new SchoolClasses(
        //     '1615629594444',
        //     'Narayana',
        //     Board.CBSE,
        //     Medium.HINDI,
        //     Class.FOURTH
        // ),
        // new SchoolClasses(
        //     '1615629594944',
        //     'Chaitanya',
        //     Board.CBSE,
        //     Medium.ENGLISH,
        //     Class.THIRD
        // ),
    ],
}

export function dashBoardReducer(state = initialDashboardState, action: DashBoardActions) {

    switch (action.type) {
        case DashBoardActionTypes.ADD_DATA_FROM_STORE:
            return {
                ...state,
                schoolClasses:[...state.schoolClasses,...action.payload],
            }
        case DashBoardActionTypes.ADD_SCHOOL_CLASS:
            return {
                ...state,
                schoolClasses: [...state.schoolClasses, action.payload]
            }
        case DashBoardActionTypes.UPDATE_A_ROW:
            return {
                ...state,
                schoolClasses: state.schoolClasses.map(
                    (el) => {
                        if (el.id == action.payload.id) {
                            return action.payload;
                        }
                        return el;
                    }),
            }
        case DashBoardActionTypes.DELETE_A_ROW:
            return {
                ...state,
                schoolClasses: state.schoolClasses.filter(
                    (el) => {
                        return el.id != action.payload.id;
                    }),
            }
        default:
            return state;
    }
}