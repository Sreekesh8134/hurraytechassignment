import { AfterViewInit, Component, ViewChild, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { Store } from '@ngrx/store';
import { AuthService } from 'src/app/common/services/auth.service';
import { AppState } from 'src/app/store/app.reducer';
import { DashboardModel, SchoolClasses } from '../../common/models';
import { DialogService } from '../../components/dialog/dialog.service';
import { AddDataFromStore, AddSchoolClass, DeleteSchoolClass, UpdateSchoolClass } from './dashboard.actions';
import { initialDashboardState } from "./dashboard.reducer";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements AfterViewInit, OnInit {
  displayedColumns: string[] = [
    'schoolName',
    'board',
    'medium',
    'class',
    'actions',
    'id',
  ];
  dataSource: MatTableDataSource<SchoolClasses>;

  // @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('paginatorRef') paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatTable, { static: true }) table: MatTable<any>;


  constructor(
    private _dialogSrvc: DialogService,
    private store: Store<AppState>,
    public _fireStoreService: AngularFirestore,
    private _authSrvc: AuthService) {
    this.dataSource = new MatTableDataSource(initialDashboardState.schoolClasses);
  }
  ngOnInit(): void {
    const fireStoreDB = this._fireStoreService.collection(`users/${this._authSrvc.userId}/dashboard`);
    let db = new DashboardModel();
    fireStoreDB.get().subscribe((data) => {
      db.schoolClasses = data.docs.map((element) => {
        return DashboardModel.fromJson(element.data())
      });
      this.store.dispatch(new AddDataFromStore(db.schoolClasses));
    })
  }


  ngAfterViewInit() {
    // console.log(this.dataSource);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.dataSource.filterPredicate =
      (data: SchoolClasses, filtersJson: string) => {
        const matchFilter = [];
        const filters = JSON.parse(filtersJson);

        filters.forEach(filter => {
          const val = data[filter.id] === null ? '' : data[filter.id];
          matchFilter.push(val.toLowerCase().includes(filter.value.toLowerCase()));
        });
        return matchFilter.every(Boolean);
      };
    this.store.select('dashboard')
      .subscribe((data) => {
        this.dataSource.data = [...data.schoolClasses];
        this.table.renderRows();
      });
  }

  deleteThirdRow() {
    this.dataSource._updateChangeSubscription();
    // this.dataSource.data.findIndex()
  }

  applyFilter(filterValue: string, colName: string) {
    const tableFilters = [];
    tableFilters.push({
      id: colName,
      value: filterValue
    });


    this.dataSource.filter = JSON.stringify(tableFilters);
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  onAddition() {
    this._dialogSrvc.openAddUpdateCard({
      isUpdate: false,
      primaryCallBack: (schoolData: SchoolClasses) => {
        this.store.dispatch(new AddSchoolClass(schoolData));
      }
    });
  }

  openDialog(type: string, el: SchoolClasses) {
    if (type == 'Delete') {
      this._dialogSrvc.open({
        title: 'Are you sure want to delete this?',
        primaryButton: 'Yes',
        secondaryButton: 'No',
        primaryCallBack: () => {
          let fireStoreDB = this._fireStoreService.collection(`users/${this._authSrvc.userId}/dashboard`).doc(`${el.id}`);
          fireStoreDB.delete();
          this.store.dispatch(new DeleteSchoolClass(el));
        },
      });
    } else {
      this._dialogSrvc.openAddUpdateCard({
        isUpdate: true,
        primaryCallBack: (schoolData: SchoolClasses) => {
          this.store.dispatch(new UpdateSchoolClass(schoolData));
          let fireStoreDB = this._fireStoreService.collection(`users/${this._authSrvc.userId}/dashboard`).doc(`${schoolData.id}`);
          fireStoreDB.update({
            // schoolName: schoolData.schoolName,
            // medium: schoolData.medium,
            // class: schoolData.class,
            // board
            ...schoolData,
          })
        },
        schoolClassData: el,
      });
    }
  }

}
