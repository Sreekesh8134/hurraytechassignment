import { Action } from "@ngrx/store";
import { SchoolClasses } from "../../common/models";

export enum DashBoardActionTypes {
    ADD_SCHOOL_CLASS = "Add a School Class Element",
    DELETE_A_ROW = "Delete an Entry from table",
    UPDATE_A_ROW = "Update an Entry from table",
    ADD_DATA_FROM_STORE="Add data from Store"
}

export class AddDataFromStore implements Action{
    readonly type= DashBoardActionTypes.ADD_DATA_FROM_STORE;

    constructor(public payload:SchoolClasses[]){}
}

export class AddSchoolClass implements Action {
    readonly type = DashBoardActionTypes.ADD_SCHOOL_CLASS;
    constructor(public payload: SchoolClasses) { }
}

export class DeleteSchoolClass implements Action {
    readonly type = DashBoardActionTypes.DELETE_A_ROW;
    constructor(public payload: SchoolClasses) { }
}

export class UpdateSchoolClass implements Action {
    readonly type = DashBoardActionTypes.UPDATE_A_ROW;
    constructor(public payload: SchoolClasses) { }
}

export type DashBoardActions = AddSchoolClass | DeleteSchoolClass | UpdateSchoolClass | AddDataFromStore;