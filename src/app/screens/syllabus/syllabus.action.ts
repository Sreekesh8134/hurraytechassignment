import { Action } from "@ngrx/store";
import { SubTopic, Syllabus, Topic } from "../../common/models";

export enum SyllabusActionTypes {
    ADD_TOPIC = "ADD TOPIC",
    ADD_SUBTOPIC = "ADD SUBTOPIC",
    SAVE_SYLLABUS = "SAVE SYLLABUS",
}


export class AddTopic implements Action {
    readonly type = SyllabusActionTypes.ADD_TOPIC;
    constructor(public payload: Topic) { }
}

export class AddSubTopic implements Action {
    readonly type = SyllabusActionTypes.ADD_SUBTOPIC;
    constructor(public payload: { subTopic: SubTopic, title: string }) { }
}

export class SaveSyllabus implements Action {
    readonly type = SyllabusActionTypes.SAVE_SYLLABUS;
    constructor(public payload: Syllabus) { }
}


export type SyllabusActions = AddTopic | AddSubTopic | SaveSyllabus;