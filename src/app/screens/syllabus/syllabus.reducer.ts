import { Syllabus, Topic } from "../../common/models";
import { SyllabusActions, SyllabusActionTypes } from "./syllabus.action";

const initialState: Syllabus = {
    board: null,
    classes: null,
    subject: null,
    syllabus_des: null,
    academicYear: null,
    topics: [],
}

export function syllabusReducer(state = initialState, action: SyllabusActions) {

    switch (action.type) {
        case SyllabusActionTypes.ADD_TOPIC:
            return {
                ...state,
                topics: [...state.topics, action.payload]
            }
        case SyllabusActionTypes.ADD_SUBTOPIC:
            let index = state.topics.findIndex((topic: Topic) => {
                console.log(topic.title);
                return topic.title == action.payload.title
            });
            // state.topics.map((topic) => {
            //     if (topic.title == action.payload.title) {
            //         return {
            //             ...state.topics[index],
            //             subTopic: [...topic.subTopic, action.payload.subTopic]
            //         }
            //     }
            //     return topic;
            // })
            // let updatedTopic: Topic = state.topics[index];
            // updatedTopic.subTopic = [...updatedTopic.subTopic, action.payload.subTopic];
            return {
                ...state,
                // 
                topics: state.topics.map((topic) => {
                    if (topic.title == action.payload.title) {
                        return {
                            ...state.topics[index],
                            subTopic: [...topic.subTopic, action.payload.subTopic]
                        }
                    }
                    return topic;
                })
            }
        case SyllabusActionTypes.SAVE_SYLLABUS:
            return {
                ...state,
                ...action.payload
            }

        default:
            return state;
    }
}