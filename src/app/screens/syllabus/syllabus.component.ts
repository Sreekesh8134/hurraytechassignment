import { Component, EventEmitter, OnDestroy, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { Board, Class, Subject, Syllabus, Topic } from 'src/app/common/models';
import { DialogService } from 'src/app/components/dialog/dialog.service';
import { AppState } from 'src/app/store/app.reducer';
import { AddTopic, SaveSyllabus } from './syllabus.action';

@Component({
  selector: 'app-syllabus',
  templateUrl: './syllabus.component.html',
  styleUrls: ['./syllabus.component.scss']
})
export class SyllabusComponent implements OnInit, OnDestroy {

  boardFormControl = new FormControl('', Validators.required);
  classFormControl = new FormControl('', Validators.required);
  subjectFormControl = new FormControl('', Validators.required);
  syllDescripFormControl = new FormControl('', [Validators.required, Validators.maxLength(150)]);
  acadYearFormControl = new FormControl('', Validators.required);
  boards: Board[] = Object.values(Board);
  classes: Class[] = Object.values(Class);
  subjects: Subject[] = Object.values(Subject);
  academic_Years: string[] = ["2018-19", "2019-20", "2020-21"];
  topicArray: Topic[] = [];

  isSaving: EventEmitter<boolean> = new EventEmitter();
  constructor(private store: Store<AppState>, private _dialogSrvc: DialogService) { }


  storeSubScription: Subscription;

  ngOnInit(): void {
    this.store.select('syllabus').subscribe((state) => {
      this.boardFormControl.patchValue(state.board);
      this.acadYearFormControl.patchValue(state.academicYear);
      this.classFormControl.patchValue(state.classes);
      this.subjectFormControl.patchValue(state.subject);
      this.syllDescripFormControl.patchValue(state.syllabus_des);
      this.topicArray = [...state.topics]
    }).unsubscribe();
    this.storeSubScription = this.store.select('syllabus').subscribe((state) => {
      this.topicArray = [...state.topics];
    });
  }

  addTopic() {
    this._dialogSrvc.openAddTopic();
  }

  topicContent($event: { topic: Topic, index: number }) {
    console.log($event.index);
    this.topicArray[$event.index] = $event.topic;
  }

  onSaving() {
    this.isSaving.emit(true);
    // if (this.boardFormControl.valid && this.acadYearFormControl.valid && this.syllDescripFormControl.valid && this.classFormControl.valid && this.subjectFormControl.valid) {
    setTimeout(() => {
      let syllabusTemplate: Syllabus = new Syllabus();
      syllabusTemplate.board = this.boardFormControl.value;
      syllabusTemplate.academicYear = this.acadYearFormControl.value;
      syllabusTemplate.classes = this.classFormControl.value;
      syllabusTemplate.subject = this.subjectFormControl.value;
      syllabusTemplate.syllabus_des = this.syllDescripFormControl.value;
      syllabusTemplate.topics = this.topicArray;
      this.store.dispatch(new SaveSyllabus(syllabusTemplate));
    }, 150);
    // }
  }

  ngOnDestroy(): void {
    this.storeSubScription.unsubscribe();
  }


}

// this.syllabusForm = this.fb.group({
    //   topics: this.fb.array([this.fb.group(
    //     {
    //       // title: this.topicFormControl,
    //       // allotedTime: this.allotedTimeFormControl,
    //       subtopics: this.fb.array([this.fb.group({
    //         subtopicTitle: '',
    //         subtopicDesc: ''
    //       })])
    //     }
    //     )])
    //   })


    // get topics() {
  //   return this.syllabusForm.get('topics') as FormArray;
  // }