import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  userNameFormCntrl = new FormControl('', [
    Validators.required,
    Validators.minLength(4),
    // Validators.maxLength(40),
    Validators.pattern('[a-zA-Z][\.a-zA-Z_-]*[a-zA-Z]')
  ]);

  passwordCntrlr = new FormControl('', [
    Validators.required,
    Validators.minLength(8),
    Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}')
  ]);

  constructor(private router: Router) { }

  ngOnInit(): void {
    // this.userNameFormCntrl.valueChanges.subscribe(() => {
    //   console.log(this.userNameFormCntrl);
    // })
  }

  onSubmit() {
    this.passwordCntrlr.updateValueAndValidity();
    this.userNameFormCntrl.updateValueAndValidity();
    if (this.userNameFormCntrl.valid && this.passwordCntrlr.valid) {
      this.router.navigate(['dashboard']);
    } else {
      //focus on errored fields
    }
  }

}
