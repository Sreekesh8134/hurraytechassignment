import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { User } from 'src/app/common/models';
import { AppState } from 'src/app/store/app.reducer';
import { AngularFireStorage } from "@angular/fire/storage";
import { AngularFirestore } from '@angular/fire/firestore';
import { AuthService } from 'src/app/common/services/auth.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { SetUser, UpdateUrl } from 'src/app/store/user/user.action';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {

  user: User;
  constructor(
    private store: Store<AppState>,
    private storage: AngularFireStorage,
    private firestore: AngularFirestore,
    private _authService: AuthService,
    public firebaseAuth: AngularFireAuth,
  ) { }

  ngOnInit(): void {
    this.store.select('user')
      .subscribe(
        (user) => this.user = user
      );
  }

  async onImageChanged(event) {
    var n = Date.now();
    const file = event.target.files[0];
    const filePath = `profileImages/${this.user.userName}_${n}`;
    const fileRef = this.storage.ref(filePath);
    await this.storage.upload(`profileImages/${this.user.userName}_${n}`, file);
    fileRef.getDownloadURL().subscribe(
      async (url) => {
        this.store.dispatch(new UpdateUrl(url));
        const userRef = this.firestore.collection('users').doc(`${this._authService.userId}`);
        userRef.update({
          photoURL: url
        })
        let currentUser = await this.firebaseAuth.currentUser;
        await currentUser.updateProfile({ photoURL: url });
        this.store.dispatch(new SetUser(User.fromJson(currentUser)));
      }
    )
  }

  get backgroundImage(): string {
    return this.user.photoURL ?
      `url(${this.user.photoURL})` :
      'url("http://simpleicon.com/wp-content/uploads/camera.png")';
  }

}
