import firebase from 'firebase/app';
import { Router } from "@angular/router";
import { Injectable } from '@angular/core';
import { AngularFireAuth } from "@angular/fire/auth";
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';

import { User } from "../models";
import { Store } from '@ngrx/store';
import { AppState } from '../../store/app.reducer';
import { SetUser } from '../../store/user/user.action';
import { DialogService } from '../../components/dialog/dialog.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  isDataLoading: boolean = true;
  private userData: any; // Save logged in user data
  // Returns true when user is looged in and email is verified
  isLoading: boolean = false;
  public get userId() {
    return this.userData.uid;
  }

  public get isLoggedIn(): boolean {
    const user = JSON.parse(localStorage.getItem('user'));
    if (!!user) {
      this.userData = JSON.parse(localStorage.getItem('user'));
      return true;
    } else {
      return false;
    }
    // return (user !== null) ? true : false;//&& user.emailVerified !== false
  }

  constructor(
    public fireStoreServc: AngularFirestore,   // Inject Firestore service
    public firebaseAuth: AngularFireAuth, // Inject Firebase auth service
    public router: Router,
    private store: Store<AppState>,
  ) {
    /* Saving user data in localstorage when 
    logged in and setting up null when logged out */
    this.firebaseAuth.onAuthStateChanged(
      (user) => {
        this.isDataLoading = false;
        if (user) {
          this.userData = user;
          localStorage.setItem('user', JSON.stringify(this.userData));
          this.store.dispatch(new SetUser(User.fromJson(user)));
        } else {
          this.userData = null;
          localStorage.setItem('user', null);
          // JSON.parse(localStorage.getItem('user'));
        }
      });
  }

  // Sign in with Google
  public googleAuth() {
    return this.authLogin(new firebase.auth.GoogleAuthProvider());
  }

  // Sign in with email/password
  public signIn(email: string, password: string) {
    this.isLoading = true;
    return this.firebaseAuth
      .signInWithEmailAndPassword(email, password)
      .then(
        (result) => {
          const userDocRef = this.fireStoreServc.collection('users').doc(result.user.uid);
          userDocRef.get().subscribe((result=>{
            console.log(result.data());
          }))
          this.router.navigate(['dashboard'], { queryParamsHandling: "preserve" });
          this.isLoading = false
        }
      ).catch(
        (error) => {
          this.isLoading = false;
          window.alert(error.message);
        }
      );
  }

  // Sign up with email/password
  public signUp(email, password, userName, phoneNumber) {
    this.isLoading = true;
    return this.firebaseAuth
      .createUserWithEmailAndPassword(email, password)
      .then(
        async (result) => {
          if (!result.user.displayName) {
            let currentUser = await this.firebaseAuth.currentUser;
            await currentUser.updateProfile({ displayName: `${userName}__${phoneNumber}` });
            let updatedUser = await this.firebaseAuth.currentUser;
            this.setUserData(updatedUser);
            this.router.navigate(['dashboard'], { queryParamsHandling: "preserve" });
          } else {
            this.setUserData(result.user);
            this.router.navigate(['dashboard'], { queryParamsHandling: "preserve" });
          }
        }
      )
      .catch(
        (error) => {
          this.isLoading = false;
          window.alert(error.message)
        }
      );
  }

  // Sign out 
  public signOut() {
    return this.firebaseAuth
      .signOut()
      .then(
        () => {
          localStorage.removeItem('user');
          this.router.navigate(['login'], { queryParamsHandling: "preserve" });
        });
  }


  // Auth logic to run auth providers
  private authLogin(provider) {
    this.isLoading = true;
    return this.firebaseAuth
      .signInWithPopup(provider)
      .then(
        (result) => {
          const userDocRef = this.fireStoreServc.collection('users').doc(result.user.uid);
          userDocRef.get().subscribe(
            (userData) => {
              console.log(userData.data());
              if (!userData.data()) {
                this.setUserData(result.user);
              }
              this.router.navigate(['dashboard'], { queryParamsHandling: "preserve" });
            }
          );
        }
      )
      .catch(
        (error) => {
          this.isLoading = false;
          window.alert(error);
        }
      );
  }

  /* Setting up user data when sign in with username/password, 
  sign up with username/password and sign in with social auth  
  provider in Firestore database using AngularFirestore + AngularFirestoreDocument service */
  private setUserData(user) {
    const userRef: AngularFirestoreDocument<any> = this.fireStoreServc.doc(`users/${user.uid}`);
    console.log(User.fromJson(user));
    return userRef.set(User.fromJson(user), {
      merge: true
    });
  }

}