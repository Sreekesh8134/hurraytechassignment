import { SchoolClasses } from ".";
export class DashboardModel {
    schoolClasses: SchoolClasses[];
    static fromJson(firedata: any): SchoolClasses {
        let schoolClass: SchoolClasses = {
            board:firedata.board,
            class:firedata.class,
            id:firedata.id,
            medium:firedata.medium,
            schoolName:firedata.schoolName
        }
        return schoolClass;
    }
}
