export enum Subject {
    MATHS = "Maths",
    ENGLISH = "English",
    PHYSICS = "Physics",
    BIOLOGY = "Biology",
    TELUGU = "Telugu",
    HINDI = 'Hindi',
    SOCIAL = "Social",
}