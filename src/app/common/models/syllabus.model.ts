import { Board, Class, Subject, Topic } from "./index";

export class Syllabus {
    board: Board;
    classes: Class;
    subject: Subject;
    syllabus_des: string;
    academicYear: number;
    topics: Topic[];
}