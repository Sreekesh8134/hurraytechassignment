export enum Class {
    NURSERY = "Nursery",
    LKG = "LKG",
    UKG = "UKG",
    ONE = "1st",
    SECOND = "2nd",
    THIRD = "3rd",
    FOURTH = "4th",
    FIFTH = "5th"
}