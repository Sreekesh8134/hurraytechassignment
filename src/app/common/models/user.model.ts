export class User {
    uid: string;
    email: string;
    userName: string;
    photoURL: string;
    phoneNumber: string;
    constructor(uid, email, userName, photoUrl?, phoneNumber?) {
        this.uid = uid;
        this.email = email;
        this.userName = userName;
        this.photoURL = photoUrl ? photoUrl : '';
        this.phoneNumber = phoneNumber ? phoneNumber : '';
    }

    static fromJson(userData: any): User {
        let arr = ['', '']
        if (userData.displayName) {
            arr = userData.displayName.split('__');
        }
        let user: User = {
            uid: userData.uid,
            email: userData.email,
            userName: arr[0],
            phoneNumber: arr[1],
            photoURL: userData.photoURL,
        }
        return user;
    }
}