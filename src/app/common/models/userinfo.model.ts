export class Userinfo{
    name:string;
    email:string;
    mobile:number;
    profileUrl:string;
    userName:string;
}