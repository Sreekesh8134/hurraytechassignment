import { Board } from './board.model';
import { Class } from './class.model';
import { Medium } from './medium.model';

export class SchoolClasses {
  schoolName: string;
  board: Board;
  medium: Medium;
  class: Class;
  id: string;

  constructor(id: string, sc: string, bo: Board, me: Medium, cl: Class) {
    this.id = id;
    this.schoolName = sc;
    this.board = bo;
    this.medium = me;
    this.class = cl;
  }
}