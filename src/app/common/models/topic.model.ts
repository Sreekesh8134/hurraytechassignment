export class Topic {
    title: string;
    allotedTime: string;
    subTopic: SubTopic[];

    constructor(title: string, allotedTime: string) {
        this.title = title;
        this.allotedTime = allotedTime;
        // subTopic: this.topicFormBuilder.array([this.topicFormBuilder.group({
        //     subtopicTitle: '',
        //     subtopicDesc: ''
        // })])
        this.subTopic = [];
    }
}

export class SubTopic {
    subtopicTitle: string;
    subtopicDesc: string;
    constructor(title: string, description: string) {
        this.subtopicTitle = title;
        this.subtopicDesc = description;
    }
}