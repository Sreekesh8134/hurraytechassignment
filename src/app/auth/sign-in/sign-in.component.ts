import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';

import { AuthService } from "../../common/services/auth.service";
import { FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {

  isLogin: boolean = false;
  current = null;
  emailFormCntrlr = new FormControl(null, [Validators.required, Validators.email]);
  phoneFormCntrlr = new FormControl(null, [Validators.required, Validators.pattern('[0-9]+'), Validators.minLength(10), Validators.maxLength(10)]);
  userNameFormCntrl = new FormControl('', [
    Validators.required,
    Validators.minLength(4),
    // Validators.maxLength(40),
    Validators.pattern('[a-zA-Z][\.a-zA-Z_-]*[a-zA-Z]')
  ]);
  passwordCntrlr = new FormControl('', [
    Validators.required,
    Validators.minLength(8),
    Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}')
  ]);
  @ViewChild("emailRef", { static: true }) emailRef: ElementRef;
  @ViewChild("passwordRef", { static: true }) passwordRef: ElementRef;
  @ViewChild("userNameRef", { static: true }) userNameRef: ElementRef;
  constructor(
    public authService: AuthService,
    private _snackBar: MatSnackBar
  ) { }

  ngOnInit() {
  }

  openSnackBar(message: string, action: string) {
    // this._snackBar.openFromComponent(SnackBarComponent, {
    //   duration: 5 * 1000,
    // });
    this._snackBar.open(message, action, {
      duration: 2000,
    })
  }

  onSubmit() {
    if (this.isLogin) {
      if (this.passwordCntrlr.valid && this.emailFormCntrlr.valid) {
        this.authService.signIn(this.emailFormCntrlr.value, this.passwordCntrlr.value);
      } else {
        this.openSnackBar("Enter Valid Details!!", "ok");
      }
    } else {
      if (this.emailFormCntrlr.valid && this.passwordCntrlr.valid && this.userNameFormCntrl.valid && this.phoneFormCntrlr.valid) {
        this.authService.signUp(this.emailFormCntrlr.value, this.passwordCntrlr.value, this.userNameFormCntrl.value, this.phoneFormCntrlr.value);
      } else {
        this.openSnackBar("Enter Valid Details!!", "ok");
      }
    }
    // if (this.emailFormCntrlr.valid && this.passwordCntrlr.valid) {
    // } else {
    //   this.emailFormCntrlr.markAllAsTouched();
    //   this.userNameFormCntrl.markAllAsTouched();
    //   this.passwordCntrlr.markAllAsTouched();
    //   if (this.emailFormCntrlr.invalid) {
    //     (<HTMLInputElement>this.emailRef.nativeElement).focus();
    //   } else {
    //     (<HTMLInputElement>this.passwordRef.nativeElement).focus();
    //   }
    // }
  }

}