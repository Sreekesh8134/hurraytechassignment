import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SignInComponent } from './sign-in/sign-in.component';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { FulfillingSquareSpinnerModule } from "angular-epic-spinners";
import { AuthService } from '../common/services/auth.service';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  declarations: [
    SignInComponent,
  ],
  imports: [
    CommonModule,
    MatFormFieldModule,
    MatCardModule,
    MatInputModule,
    MatButtonModule,
    FormsModule,
    ReactiveFormsModule,
    MatSnackBarModule,
    FulfillingSquareSpinnerModule,
  ],
  exports: [
    SignInComponent,
  ],
  providers: [
    AuthService,
  ]
})
export class AuthModule { }
