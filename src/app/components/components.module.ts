import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SidenavComponent } from './sidenav/sidenav.component';
import { DialogComponent } from './dialog/dialog.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { A11yModule } from '@angular/cdk/a11y';
import { MatSelectModule } from '@angular/material/select';
import { DashboardCardComponent } from './dashboard-card/dashboard-card.component';
import { MatCardModule } from "@angular/material/card";
import { TopicComponent } from './topic/topic.component';
import { MatIconModule } from '@angular/material/icon';
import { TopicDialogComponent } from './topic-dialog/topic-dialog.component';

@NgModule({
  declarations: [SidenavComponent, DialogComponent, DashboardCardComponent, TopicComponent, TopicDialogComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatDialogModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    A11yModule,
    MatSelectModule,
    MatCardModule,
    MatIconModule,
  ],
  exports: [TopicComponent]
})
export class ComponentsModule { }
