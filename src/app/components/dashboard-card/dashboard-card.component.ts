import { Component, Inject, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Board, Class, Medium, SchoolClasses } from 'src/app/common/models';
import { AuthService } from 'src/app/common/services/auth.service';
import { DialogAddUpdate } from '../dialog/dialog.model';

@Component({
  selector: 'app-dashboard-card',
  templateUrl: './dashboard-card.component.html',
  styleUrls: ['./dashboard-card.component.scss']
})
export class DashboardCardComponent implements OnInit {

  schoolNameFormCntrl = new FormControl('', Validators.required);
  boardFormControl = new FormControl('', Validators.required);
  classFormControl = new FormControl('', Validators.required);
  mediumFormControl = new FormControl('', Validators.required);
  boards: Board[] = Object.values(Board);
  classes: Class[] = Object.values(Class);
  mediums: Medium[] = Object.values(Medium);


  constructor(
    public dialogRef: MatDialogRef<DashboardCardComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogAddUpdate,
    public _fireStoreServc: AngularFirestore,
    private _authService:AuthService,
  ) { }

  ngOnInit(): void {
    if (this.data.isUpdate) {
      this.prefillValues();
    }
  }

  onAdd() {
    if (this.schoolNameFormCntrl.valid && this.boardFormControl.valid && this.classFormControl.valid && this.mediumFormControl.valid) {
      let schoolClassData: SchoolClasses = {
        id: this.data.isUpdate ? this.data.schoolClassData.id : `${(new Date()).getTime()}`,
        schoolName: this.schoolNameFormCntrl.value,
        medium: this.mediumFormControl.value,
        board: this.boardFormControl.value,
        class: this.classFormControl.value,
      }
      this.data.primaryCallBack(schoolClassData);
      const fireStoreDB=this._fireStoreServc.collection(`users/${this._authService.userId}/dashboard`).doc(`${schoolClassData.id}`);
      fireStoreDB.set(schoolClassData);
      this.dialogRef.close();
    }
  }
  onNoClick(): void {
    this.dialogRef.close();
  }

  prefillValues() {
    this.schoolNameFormCntrl.patchValue(this.data.schoolClassData.schoolName);
    this.boardFormControl.patchValue(this.data.schoolClassData.board);
    this.classFormControl.patchValue(this.data.schoolClassData.class);
    this.mediumFormControl.patchValue(this.data.schoolClassData.medium);
  }


}
