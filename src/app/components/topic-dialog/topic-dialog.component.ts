import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { Topic } from 'src/app/common/models';
import { AppState } from 'src/app/store/app.reducer';
import { AddSubTopic, AddTopic } from '../../screens/syllabus/syllabus.action';
import { DialogComponent } from '../dialog/dialog.component';
import { DialogData } from '../dialog/dialog.model';

@Component({
  selector: 'app-topic-dialog',
  templateUrl: './topic-dialog.component.html',
  styleUrls: ['./topic-dialog.component.scss']
})
export class TopicDialogComponent implements OnInit {

  titleFormControl = new FormControl('', Validators.required);
  allotedFormControl = new FormControl('', Validators.required);
  constructor(private store: Store<AppState>, public dialogRef: MatDialogRef<TopicDialogComponent>) { }

  ngOnInit(): void {
  }

  onAdd() {
    if (this.titleFormControl.valid && this.allotedFormControl.value) {
      let newTopic = new Topic(this.titleFormControl.value, this.allotedFormControl.value);
      this.store.dispatch(new AddTopic(newTopic));
      this.dialogRef.close();
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
