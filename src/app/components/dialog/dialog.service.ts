import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef, } from '@angular/material/dialog';
import { Observable } from 'rxjs';

import { DialogAddUpdate, DialogData } from "./dialog.model";;
import { DialogComponent } from "./dialog.component";
import { DashboardCardComponent } from '../dashboard-card/dashboard-card.component';
import { SchoolClasses } from 'src/app/common/models';
import { TopicDialogComponent } from '../topic-dialog/topic-dialog.component';

@Injectable({
  providedIn: 'root'
})
export class DialogService {

  constructor(private dialog: MatDialog) { }

  dialogRef: MatDialogRef<DialogComponent>;
  dialogRefAdd: MatDialogRef<DashboardCardComponent>;
  dialogTopic:MatDialogRef<TopicDialogComponent>

  public open(options: DialogData) {
    this.dialogRef = this.dialog.open(DialogComponent, {
      data: options,
      disableClose: true,
      maxWidth: "350px",
    });
  }

  public openAddUpdateCard(options: DialogAddUpdate) {
    this.dialogRefAdd = this.dialog.open(DashboardCardComponent, {
      disableClose: true,
      minWidth: "350px",
      data: options,
    });
  }

  public openAddTopic(){
    this.dialogTopic=this.dialog.open(TopicDialogComponent,{
      disableClose: true,
      minWidth: "350px",
    })
  }

  public confirmed() {
    return this.dialogRef.afterClosed();
  }
}
