import { SchoolClasses } from "src/app/common/models";

export class DialogData {
    title: string;
    message?: string;
    primaryButton: string;
    secondaryButton?: string;
    inputFieldLabel?: string;
    primaryCallBack?: Function;
}

export class DialogAddUpdate {
    isUpdate: boolean;
    primaryCallBack: Function;
    schoolClassData?: SchoolClasses;
}