import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { SubTopic, Topic } from 'src/app/common/models';
import { AppState } from 'src/app/store/app.reducer';
import { AddSubTopic } from '../../screens/syllabus/syllabus.action';

@Component({
  selector: 'app-topic',
  templateUrl: './topic.component.html',
  styleUrls: ['./topic.component.scss']
})
export class TopicComponent implements OnInit {

  @Input() topic: Topic;
  @Input() index: number;
  @Input() isSaving: EventEmitter<boolean>;
  @Output() topicDialog = new EventEmitter<{ topic: Topic, index: number }>();

  titleFormControl = new FormControl('', Validators.required);
  allotedTimeFormControl = new FormControl('', Validators.required);
  subTopicFormControl = new FormControl('', Validators.required)

  constructor(private formBuilder: FormBuilder, private store: Store<AppState>) { }

  topicForm: FormGroup;

  ngOnInit(): void {

    console.log(this.topic);
    this.topicForm = this.formBuilder.group({
      title: this.titleFormControl,
      allotedTime: this.allotedTimeFormControl,
      subTopic: this.formBuilder.array([]),
    });
    if (this.topic.subTopic.length == 0) {
      let formArray: FormArray = this.topicForm.get('subTopic') as FormArray;
      formArray.push(this.formBuilder.group({
        subtopicTitle: '',
        subtopicDesc: ''
      }))
    } else {
      for (let i = 0; i < this.topic.subTopic.length; i++) {
        const el: SubTopic = <SubTopic>this.topic.subTopic[i];
        let formArray: FormArray = this.topicForm.get('subTopic') as FormArray;
        formArray.push(this.formBuilder.group({
          subtopicTitle: el.subtopicTitle,
          subtopicDesc: el.subtopicDesc,
        }));
      }
    }
    // this.topicForm.get('title').disable();
    this.isSaving.subscribe(() => {
      this.topicDialog.emit({ topic: this.topicForm.value, index: this.index })
    });
    this.titleFormControl.patchValue(this.topic.title);
    this.allotedTimeFormControl.patchValue(this.topic.allotedTime);
  }


  get subtopics() {
    return this.topicForm.get('subTopic') as FormArray;
  }

  addSubTopic() {
    this.subtopics.push(this.formBuilder.group({
      subtopicTitle: '',
      subtopicDesc: ''
    }));
    // let newTi = new SubTopic("dsfsf", "sdfsdfg");
    // this.store.dispatch(new AddSubTopic({ subTopic: newTi, title: this.titleFormControl.value }));
  }

  deleteSubtopic(index) {
    this.subtopics.removeAt(index);
  }

  onSaving() {

  }

}
