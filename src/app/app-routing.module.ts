import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccountComponent } from './screens/account/account.component';
import { DashboardComponent } from './screens/dashboard/dashboard.component';
import { LoginComponent } from './screens/login/login.component';
import { SyllabusComponent } from './screens/syllabus/syllabus.component';
import { SidenavComponent } from "./components/sidenav/sidenav.component";
import { TopicComponent } from './components/topic/topic.component';
import { SignInComponent } from "./auth/sign-in/sign-in.component";
import { AuthGuardService } from "./common/services/auth-guard.service";

const routes: Routes = [
  { path: "", redirectTo: "login", pathMatch: "full" },
  { path: "login", component: SignInComponent },
  { path: "dashboard", component: DashboardComponent, canActivate: [AuthGuardService] },
  { path: "account", component: AccountComponent, canActivate: [AuthGuardService] },
  { path: "syllabus", component: SyllabusComponent, canActivate: [AuthGuardService] },
  // { path: "sidenav", component: SidenavComponent },
  // { path: "topic", component: TopicComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
