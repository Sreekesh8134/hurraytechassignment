// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  // For Firebase JS SDK v7.20.0 and later, measurementId is optional
  firebaseConfig : {
    apiKey: "AIzaSyAQ_DbDTlrNeIaU3KDuIaUV1gwMlk3GMpI",
    authDomain: "hurray-tech-assignment.firebaseapp.com",
    projectId: "hurray-tech-assignment",
    storageBucket: "hurray-tech-assignment.appspot.com",
    messagingSenderId: "423876839839",
    appId: "1:423876839839:web:11164315804c23ca92d5aa",
    measurementId: "G-V20JZVM2DQ"
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
